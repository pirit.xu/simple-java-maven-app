#!/usr/bin/env groovy

properties(
  [
    buildDiscarder(
      logRotator(
        artifactDaysToKeepStr: '5',
        artifactNumToKeepStr: '1',
        daysToKeepStr: '30',
        numToKeepStr: '20'
      )
    ),
    disableConcurrentBuilds(),
    gitLabConnection('simple-java-maven-app.git'),
    pipelineTriggers(
      [[
        $class: "GitLabPushTrigger",
        triggerOnPush: true,
        triggerOnMergeRequest: true,
        triggerOpenMergeRequestOnPush: "never",
        triggerOnNoteRequest: true,
        noteRegex: "Jenkins please retry a build",
        skipWorkInProgressMergeRequest: true,
        ciSkip: true,
        setBuildDescription: true,
        // branchFilterType: "All"
        branchFilterType: "NameBasedFilter",
        includeBranchesSpec: "develop",
        excludeBranchesSpec: ""
      ]]
    )
  ]
)

// Global variables
currentStageName = ''

// ### Project-Specific Configuration ##

// Jenkins - Replace values within "<< >>" with project specific ones
def jenkinsDockerImage = 'base'

// Components
def majorMinorVersion = '1.0'
def serverlessType = 'sam' // allowed values: 'sam' or 'serverless'

// GIT
def gitSource = 'https://gitlab.com/pirit.xu/simple-java-maven-app.git'
def masterBranchName = 'develop'
def gitHash
def branchName
def gitCredentialsId = 's.pe.cibuild_ssh'
def deploying = true
def gitSubModules = true
def tag = params.tag


try {
  node {
    echo '\u2600 Environment Variables'
    sh "env"

    stage('Preparation') {
      echo '\u2756 Preparation'
      echo '\u2600 parameter tag = ' + tag
      if (tag == null || tag == '') {
        echo '\u27A1 Checkout branch develop'
        gitSubModules = true
      } else {
        echo '\u27A1 Checkout by tag ' + tag
        gitSubModules = false
      }

      if (gitSubModules) {
        checkout(
          [
            $class: 'GitSCM',
            branches:
              [[name: '*/develop']],
            doGenerateSubmoduleConfigurations: false,
            extensions:
              [[
                 $class: 'SubmoduleOption',
                 disableSubmodules: false,
                 parentCredentials: true,
                 recursiveSubmodules: true,
                 reference: '',
                 trackingSubmodules: false
              ]],
            gitTool: 'Default',
            submoduleCfg: [],
            userRemoteConfigs:
              [[
                credentialsId: gitCredentialsId,
                url: gitSource
              ]]
          ]
        )
      } else {
        // git credentialsId: gitCredentialsId, poll: false, url: gitSource
        checkout scm: 
        [
          $class: 'GitSCM',
          branches: 
            [[
              name: 'refs/tags/' + tag
            ]],
          doGenerateSubmoduleConfigurations: false,
          extensions:
            [[
              $class: 'SubmoduleOption',
              disableSubmodules: false,
              parentCredentials: true,
              recursiveSubmodules: true,
              reference: '',
              trackingSubmodules: false
            ]],
          gitTool: 'Default',
          submoduleCfg: [],
          userRemoteConfigs: 
            [[
              credentialsId: gitCredentialsId,
              url: gitSource
            ]]
        ], 
        poll: false
      }

      currentStageName = 'Preparation'
      echo "\u2600 BUILD_URL=${env.BUILD_URL}"
      def workspace = pwd()
      echo "\u2600 workspace=${workspace}"

      gitHash = ((env.currentCommitHash != null) ? env.currentCommitHash : (sh (returnStdout: true, script: 'git rev-parse --short HEAD').trim()))

      version="${majorMinorVersion}.${env.BUILD_NUMBER}"
    
    }

    stage('Build') {
      echo '\u2756 Build'

      // Run Build Commands
      echo "\u2600 Creating ${serverlessType} Package"
      docker.image('maven:3.3.3').inside {
           stage('Test') {
               // sh 'mvn package -s settings.xml -DskipTests'
               sh 'mvn package -DskipTests'
           }
       }
           
    }

    currentStageName = 'Harvesting S3 files'

    stage('AWS Uploads') {
      echo '\u2756 AWS Uploads'
      currentStageName = 'AWS Uploads'
    }
  }
  currentBuild.result = 'SUCCESS'
} catch (exc) {
  currentBuild.result = 'FAILURE'
  throw exc
}
